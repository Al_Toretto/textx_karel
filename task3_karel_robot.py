from textx import metamodel_from_file
from karel_robot.run import *


class KarelRobot(object):

    DEFAULT_CONTEXT = {"break": False}

    def __init__(self) -> None:
        self.stack_frames = [dict(KarelRobot.DEFAULT_CONTEXT)]
        set_speed(50)
    
    def process_command(self, command):
        if command.__class__.__name__ == "Turn":
            if command.direction == "right":
                turn_right()
            elif command.direction == "left":
                turn_left()
        elif command.__class__.__name__ == "Beeper":
            if command.action == "put":
                put_beeper()
            elif command.action == "pick":
                pick_beeper()
        elif command.__class__.__name__ == "ProcedureCall":
            self.process_subroutine_call(command.name, command.parameters)
        elif command.__class__.__name__ == "Return":
            self.stack_frames[-2]['return'] = command.logical_expression
        elif command == "move":
            move()
        elif command == "exit":
            exit()
        elif command == "break":
            self.stack_frames[-1]['break'] = True
        


    def process_subroutine_call(self, subroutine, parameters):
        self.stack_frames += [dict(KarelRobot.DEFAULT_CONTEXT)]
        for i, v in enumerate(parameters):
            self.stack_frames[-1][vars(self)[subroutine.name][0][i]] = v
        self.process_statements(vars(self)[subroutine.name][1])
        self.stack_frames = self.stack_frames[:-1]


    def process_subroutine_definition(self, definition):
        if definition.__class__.__name__ == "Procedure" or definition.__class__.__name__ == "Function":
            vars(self)[definition.name] = (definition.parameters, definition.body)

    def process_logical_expression(self, logical_expression):
        if logical_expression == "front_is_blocked":
            return front_is_blocked()
        elif logical_expression == "is_beeper":
            return beeper_is_present()
        elif logical_expression == "front_is_treasure":
            return front_is_treasure()
        
        elif logical_expression == "north":
            return facing_north()
        elif logical_expression == "south":
            return facing_south()
        elif logical_expression == "east":
            return facing_east()
        elif logical_expression == "west":
            return facing_west()
        
        elif logical_expression.__class__.__name__ == "Not":
            return not self.process_logical_expression(logical_expression.logical_expression)
        elif logical_expression.__class__.__name__ == "And":
            return self.process_logical_expression(logical_expression.left_operand) and self.process_logical_expression(logical_expression.right_operand)
        elif logical_expression.__class__.__name__ == "Or":
            return self.process_logical_expression(logical_expression.left_operand) or self.process_logical_expression(logical_expression.right_operand)
        elif logical_expression.__class__.__name__ == "Argument":
            return self.process_logical_expression(self.stack_frames[-1][logical_expression.name])
        elif logical_expression.__class__.__name__ == "FunctionCall":
            self.process_subroutine_call(logical_expression.name, logical_expression.parameters)
            return self.process_logical_expression(self.stack_frames[-1]['return'])
    
    
    def process_statements(self, statements):
        for statement in statements:
            self.process_statement(statement)
            if self.stack_frames[-1]['break']:
                break
        
        
    def process_statement(self, statement):
        if self.stack_frames[-1]['break']:
            print("i think this is useless")
            return
        self.process_subroutine_definition(statement)

        self.process_command(statement)
        if statement.__class__.__name__ == "IfStatement":
            if self.process_logical_expression(statement.condition):
                self.process_statements(statement.if_body_statements)
            else:
                self.process_statements(statement.else_body_statements)
        elif statement.__class__.__name__ == "WhileStatement":
            while self.process_logical_expression(statement.condition):
                self.process_statements(statement.statements)
                if self.stack_frames[-1]['break']:
                    self.stack_frames[-1]['break'] = False
                    break
        elif statement.__class__.__name__ == "RepeatStatement":
            for i in range(int(statement.count)):
                self.process_statements(statement.statements)
                if self.stack_frames[-1]['break']:
                    self.stack_frames[-1]['break'] = False
                    break

    def interpret(self, model):
        self.process_statements(model.statements)

karel_mm = metamodel_from_file('task3_karel_grammer.tx')
karel_model = karel_mm.model_from_file('task3_karel_example.karel')

robot = KarelRobot()
robot.interpret(karel_model)


